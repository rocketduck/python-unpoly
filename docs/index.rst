Welcome to Unpoly's documentation!
==================================

Unpoly is a framework agnostic python library implementing the `Unpoly server protocol <https://unpoly.com/up.protocol>`_.
It currently ships with adapters for `Django <https://djangoproject.com>`_ but can be easily integrated with other frameworks
as well by writing a small :ref:`adapter<adapters>`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage.rst
   adapters.rst
   integrations.rst
   changes.md


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
