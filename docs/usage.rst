Usage
=====

.. automodule:: unpoly.up

.. autoclass:: unpoly.up.Unpoly
    :members:
    :special-members: __bool__

.. autoclass:: unpoly.up.Cache
    :members:

.. autoclass:: unpoly.up.Layer
    :members:
