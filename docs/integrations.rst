Integrations
============

.. _django:

Django
------

Add :class:`unpoly.contrib.django.UnpolyMiddleware` to `MIDDLEWARES` and it will
attach :class:`unpoly.up.Unpoly` as `up` to every request. As simple as that ;=)
